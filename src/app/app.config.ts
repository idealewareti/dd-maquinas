export const AppConfig = {
    DOMAIN: 'ddmaquinas',
    NAME: 'DD Máquinas',
    FACEBOOK_PAGE: 'duplormaquinas',
    LOADER_COLOR: '#333333',
    PATH: "0.0.0.0",
    PORT: 4010,
    KONDUTO: false
}