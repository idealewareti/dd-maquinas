import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { PLATFORM_ID, Inject, Input } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Store } from '../../../models/store/store';
import { Brand } from '../../../models/brand/brand';
import { AppCore } from '../../../app.core';
import { BrandManager } from '../../../managers/brand.manager';

declare var $: any;

@Component({
    selector: 'brand-nav',
    templateUrl: '../../../templates/home/brand-nav/brand-nav.html',
    styleUrls: ['../../../templates/home/brand-nav/brand-nav.scss']
})
export class BrandNavComponent implements OnInit, AfterViewChecked {
    @Input() store: Store;
    brands: Brand[] = [];

    constructor(
        private manager: BrandManager,
        @Inject(PLATFORM_ID) private platformId: Object,
    ) { }

    ngOnInit() {
        this.manager.getAll()
            .subscribe(brands => {
                this.brands = brands;
            },error => {
                throw new Error(`${error.error} Status: ${error.status}`);
            });
    }

    ngAfterViewChecked() {
        if (isPlatformBrowser(this.platformId)) {
            if (this.brands
                && this.brands.length > 0
                && $(`.brand-slider`).children('li').length > 1
                && $(`.brand-slider`).children('.owl-stage-outer').length == 0
            ) {
                $(`.brand-slider`).owlCarousel({
                    items: 6,
                    margin: 10,
                    loop: true,
                    nav: false,
                    navText: [
                        '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                        '<i class="fa fa-angle-right" aria-hidden="true"></i>'
                    ],
                    dots: false,
                    autoplay: true,
                    navElement: 'div',
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                    responsive: {
                        0: { items: 2 },
                        768: { items: 3 },
                        992: { items: 5 },
                        1200: { items: 5 }
                    }
                });
            }
        }
    }

    getRoute(brand: Brand): string {
        return `/marcas/${AppCore.getNiceName(brand.name)}-${brand.id}`;
    }

    getPictureUrl(brand: Brand): string {
        return `${this.store.link}/static/brands/${brand.picture}`;
    }

    getBrands() {
        if (!isPlatformBrowser(this.platformId)) {
            return this.brands.slice(0, 5);
        }
        return this.brands;
    }
}