import { Component, Input } from '@angular/core';
import { ShowCaseBanner } from '../../../models/showcase/showcase-banner';
import { Store } from '../../../models/store/store';

@Component({
    selector: 'showcase-banner-half',
    templateUrl: '../../../templates/home/showcase-banner-half/showcase-banner-half.html',
    styleUrls: ['../../../templates/home/showcase-banner-half/showcase-banner-half.scss']
})
export class ShowcaseBannerHalfComponent {
    @Input() banners: ShowCaseBanner[];
    @Input() store: Store;
    @Input() typebanner: string;

    getMiniBanners(banners: ShowCaseBanner[], type: string) {
        if( banners ){
            var miniBannerReturn = [];
            var miniBannerShowcaseReturn = [];
            for( var x = 0; x < banners.length; x++ ){
                if( banners[x]['name'].indexOf('mini-banner-showcase') >= 0 ) {
                    miniBannerShowcaseReturn.push(banners[x]);
                }else{
                    miniBannerReturn.push(banners[x]);
                }
            }
            if( type == 'mini-banners'){
                return miniBannerReturn; 
            }else if( type == 'showcase' ){
                return miniBannerShowcaseReturn;
            }
        }
    }

    getBannerUrl(banner: ShowCaseBanner): string {
        return `${this.store.link}/static/showcases/${banner.fullBanner}`;
    }
}